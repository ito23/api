const value=process.env.ORIGIN
const origin=value??'*'

export default {
    origin,
    methods: 'GET,HEAD,PUT,POST',
    preflightContinue: false,
    optionsSuccessStatus: 204,
}