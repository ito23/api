import {Router} from 'express';
import AuthController from '../app/controllers/AuthController';
import booksRouter from './api/books';
import usersRouter from './api/users';
import validate from '../app/middleware/validate';
import loginSchema from '../app/schemas/loginSchema';
import Auth from '../app/middleware/auth';

const apiRouter:Router=Router();

apiRouter.post('/v1/login',[validate(loginSchema)],AuthController.login);
apiRouter.use('/v1/books',[Auth], booksRouter)
apiRouter.use('/v1/users',[Auth], usersRouter)

export default apiRouter;