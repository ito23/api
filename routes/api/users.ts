import {Router} from 'express';
import UserController from '../../app/controllers/UserController';
import validate from '../../app/middleware/validate';
import userCreateSchema from '../../app/schemas/userCreateSchema';

const usersRouter:Router=Router();

usersRouter.get('/', UserController.index);
usersRouter.post('/', [validate(userCreateSchema)],UserController.store);
usersRouter.get('/:user_id/transactions', UserController.transactions);


export default usersRouter;