import {Router} from 'express';
import BookController from '../../app/controllers/BookController';
import validate from '../../app/middleware/validate';
import bookCreateSchema from '../../app/schemas/bookCreateSchema';

const booksRouter:Router=Router();

booksRouter.get('/', BookController.index);
booksRouter.post('/',[validate(bookCreateSchema)], BookController.store);
booksRouter.get('/:id', BookController.show);
booksRouter.post('/:id/transactions', BookController.requestBook);
booksRouter.put('/:id/transactions/:transaction_id', BookController.returnBook);


export default booksRouter;