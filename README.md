# API-ULibrary

Snack store

## Pre-requisites
- Node v14.17.6
- PostgreSQL 14

## Installation
1. Create a copy of `.env.example` with name `.env`
in the root of the project, and set your credentials.

```bash
cp .env.example .env
```

### Enviroment variables
| Variables| Descripción|  
| ----------- | -----------|
|APP_NAME                             |The system name
|HOST 	                                 |Host used to run the application.
|PORT                                    |Port used to run the application.
|DB_HOST                                 |Db Host.
|DB_PORT                              |Db port
|DB_NAME                               |Db name
|DB_USERNAME                     |Db username
|DB_PASSWORD                 |Db password
|SECRET_KEY                 |Secret key by token
|ORIGIN                               |Cors origin 


## Commands

Use the node module manager npm to install dependencies.

```bash
npm install
```

Compile the project and run the project with nodemon
```bash
npm run dev
``` 
Compile project:

```bash
npm run build
``` 
Run project: 
```bash
npm run start
``` 
Compile documentation:
```bash
npm run swagger
``` 
## Go to API
[http://api-library.robertoreyes.online/](http://api-library.robertoreyes.online/)