import {Request, Response, NextFunction} from 'express'
import Jwt from 'jsonwebtoken';
import IToken from '../interfaces/IToken';
import { User } from '../models';

const Auth=async(req:Request, res:Response, next:NextFunction)=>{
    try{
        const {authorization}=req.headers;

        if(!authorization) return res.status(401).json({
            message:'unauthorized'
        })
        const secret=process.env.SECRET_KEY;
        if(!secret) return res.status(500).json({
            message:'Internal Error config-key'
        })
        const token:string=authorization.replace('Bearer ', '');
        const { id }=Jwt.verify(token, secret as string) as IToken

        const user=await User.findByPk(id)
        req.user =user as User;
        next();
    }catch(e){
        res.status(401).json({
            message:'unauthorized'
        })
    }
}

export default Auth;