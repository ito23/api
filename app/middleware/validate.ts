import { Request, Response, NextFunction } from "express";
type Type = 'body' | 'query';
export default function validate(schema:any, type:Type='body'){
    return async(req:Request, res: Response,next: NextFunction)=>{
        try{
            await schema.validateAsync(req[type]);
            next();
        }catch(e:any){
            
            return res.status(400).json({
                message: 'Wrong body request',
                fields:e.details.map((err:any) => ({
                    message: err.message.replaceAll('"', ''),
                    field: err.context.key,
                  }))
            });
        }
    }
}