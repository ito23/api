import { Sequelize } from 'sequelize';

import dotenv from 'dotenv';
dotenv.config();

export default class DB{
    static connection():Sequelize{
        
        const DB_HOST= process.env.DB_HOST || 'localhost'
        const DB_NAME=process.env.DB_NAME || 'api'
        const DB_USERNAME=process.env.DB_USERNAME || 'admin'
        const DB_PASSWORD=process.env.DB_PASSWORD || 'admin'
        const DB_PORT=parseInt(process.env.DB_PORT || '5432')

        return new Sequelize(DB_NAME, DB_USERNAME, DB_PASSWORD,{
            host:DB_HOST,
            port: DB_PORT,
            dialect: 'postgres'
        })
    }
}