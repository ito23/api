import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import apiRouter from '../../routes/api';
import swagger from "swagger-ui-express";
import YAML from 'yamljs'
import path from 'path';
import cors from 'cors'
import corsConfig from '../../configs/cors'


dotenv.config();

export default class Server {
    private app: Express;
    private port: number;
    private host: string;

    constructor() {
        const PORT=process.env.PORT || '8000';
        const HOST=process.env.HOST || '0.0.0.0';
        this.port = parseInt(PORT);
        this.host = HOST;
        this.app = express();
        this.middleware();
        this.routes();
    }

    private middleware(): void {
        this.app.use(cors(corsConfig))
        this.app.use(express.static('public'))
        this.generateDocs();
        this.app.use(express.json())
    }

    private generateDocs(){
        const swaggerDocumentPublic = YAML.load(path.join(__dirname, '../../docs/index.yaml'));
        
        this.app.use('/docs', swagger.serveFiles(swaggerDocumentPublic), swagger.setup(swaggerDocumentPublic))
    }

    private routes(): void {
        this.app.use('/api', apiRouter)
        
    }

    public start(): void {
        this.app.listen(this.port, this.host, () => {
            console.log(`http://${this.host}:${this.port}`)
        });
    }
}