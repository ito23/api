import Joi from "joi";

export default Joi.object({
    title:Joi.string().required(),
    author:Joi.string().required(),
    genre:Joi.string().required(),
    stock:Joi.number().required(),
    year:Joi.number().required()
})