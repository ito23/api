import Joi from "joi";

export default Joi.object({
    first_name:Joi.string().required(),
    last_name:Joi.string().required(),
    email:Joi.string().required().email(),
    role:Joi.string().required().valid('Student','Librarian'),
})