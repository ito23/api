import { Request, Response } from 'express'
import { Book, Transaction, User } from '../models';
import { Op } from 'sequelize';

type Filters = {
    first_name?: object;
    last_name?: object;
    email?: object;
}

export default class UserController {
    public static async index(req: Request, res: Response) {
        const { first_name, last_name, email } = req.query;
        let filters: Filters = {};

        if (first_name) filters.first_name = {
            [Op.iLike]: `%${first_name}%`
        }

        if (last_name) filters.last_name = {
            [Op.iLike]: `%${last_name}%`
        }

        if (email) filters.email = {
            [Op.iLike]: `%${email}%`
        }

        const users = await User.findAll({
            where: filters
        });

        return res.status(200).json(users)
    }

    public static async store(req: Request, res: Response) {
        const { first_name, last_name, email, role } = req.body;
        const exist = await User.findOne({
            where: {
                email
            }
        })
        if(exist) return res.status(422).json({
            message:'email already exists'
        })
        const user = await User.create({
            first_name,
            last_name,
            email,
            role
        })

        return res.status(201).json(user)
    }


    public static async transactions(req: Request, res: Response) {
        const { user_id: userId } = req.params;

        const user = await User.findByPk(userId)
        if (!user) return res.status(404).json({
            message: "User doesn't exist"
        })
        return res.status(200).json(await user.getStudentTransactions({
            include: {
                model: Book
            }
        }))
    }
}