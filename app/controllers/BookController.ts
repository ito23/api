import { Request, Response } from 'express'
import { Book, Transaction } from '../models'
import { Op } from 'sequelize';
import DB from '../cors/DB';
type Filters = {
    title?: object;
    author?: object;
    genre?: object;
}
export default class BookController {
    public static async index(req: Request, res: Response) {
        const { title, author, genre } = req.query;
        let filters: Filters = {};

        if (title) filters.title = {
            [Op.iLike]: `%${title}%`
        }

        if (author) filters.author = {
            [Op.iLike]: `%${author}%`
        }

        if (genre) filters.genre = {
            [Op.iLike]: `%${genre}%`
        }

        const books = await Book.findAll({
            where: filters,
        })

        return res.status(200).json(books)
    }

    public static async store(req: Request, res: Response) {
        const { title, author, genre, year, stock } = req.body;

        const book = await Book.create({
            title, author, genre, year, stock
        })

        return res.status(201).json(book)
    }

    public static async show(req: Request, res: Response) {
        const { id } = req.params;

        const book = await Book.findByPk(id);
        if (!book) return res.status(404).json({
            message: "Book doesn't exist"
        })
        return res.status(200).json(book)
    }

    public static async requestBook(req: Request, res: Response) {
        const t = await DB.connection().transaction();
        try {
            const { id } = req.params;
            const book = await Book.findByPk(id);

            if (!book) return res.status(404).json({
                message: "Book doesn't exist"
            })

            const request = await Transaction.create({
                book_id: book?.id,
                student_id: req.user.id,
                date_out: new Date()
            }, {
                transaction: t
            })

            await book.update({
                stock: --book.stock
            }, {
                transaction: t
            });

            await t.commit();
            return res.status(200).json(request);
        } catch {
            await t.rollback();
            return res.status(500).json({
                message: 'Internal Error Server'
            })
        }

    }

    public static async returnBook(req: Request, res: Response) {
        const t = await DB.connection().transaction();
        try {
            const { transaction_id } = req.params;
            const transaction = await Transaction.findByPk(transaction_id);

            const book = await Book.findByPk(transaction?.book_id)
            await book?.update({
                stock: ++book.stock,
            }, {
                transaction: t
            })

            await transaction?.update({
                id_librarian: req.user.id,
                entry_date: new Date()
            }, {
                transaction: t
            });
            await t.commit();
            return res.status(200).json(transaction);
        } catch {
            await t.rollback();
            return res.status(500).json({
                message: 'Internal Error Server'
            })
        }
    }
}