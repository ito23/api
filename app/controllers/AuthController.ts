import {Request, Response} from 'express'
import { User } from '../models';
import jwt from 'jsonwebtoken';

export default class AuthController{
    public static async login(req:Request, res:Response){
        const {email}=req.body;
        const user=await User.findOne({
            where:{
                email
            },
            attributes:['id','email', 'role']
        })
        if(!user){
            return res.status(401).json({
                message:'unauthorized'
            })
        }

        const secret=process.env.SECRET_KEY
        if(!secret) return res.status(500).json({
            message:'Internal Error config-key'
        })
        
        const token=jwt.sign(user?.toJSON(), secret)
        return res.status(201).json({
            token
        })
    }
}