import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional, HasManyGetAssociationsMixin } from "sequelize";
import DB from "../cors/DB";
import Transaction from "./Transaction";

export default class User extends Model<InferAttributes<User>, InferCreationAttributes<User>>{
   declare id: CreationOptional<number>;
   declare first_name:string;
   declare last_name:string;
   declare email:string;
   declare role:string;
   declare getStudentTransactions:HasManyGetAssociationsMixin<Transaction>

   public static associate(): void {
    this.hasMany(Transaction,{
        foreignKey:'student_id',
        as:'StudentTransactions'
    })

    this.hasMany(Transaction,{
        foreignKey:'id_librarian',
        as:'LibrarianTransactions'
    })
 }
}

User.init({
    id:{
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement:true,
        primaryKey:true
    },
    first_name:{
        type:DataTypes.STRING,
        allowNull:false
    },
    last_name:{
        type:DataTypes.STRING,
        allowNull:false
    },
    email:{
        type:DataTypes.STRING,
        allowNull:false
    },
    role:{
        type:DataTypes.STRING,
        allowNull:false
    }
},{
    sequelize: DB.connection(),
    tableName: 'users',
    timestamps:false,
});