import Book from "./Book";
import User from "./User";
import Transaction from "./Transaction";

Book.associate();
User.associate()
Transaction.associate();

export {
    Book,
    User,
    Transaction
}