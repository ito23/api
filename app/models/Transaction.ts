import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional, BelongsToGetAssociationMixin} from "sequelize";
import DB from "../cors/DB";
import Book from "./Book";
import User from "./User";

export default class Transaction extends Model<InferAttributes<Transaction>, InferCreationAttributes<Transaction>>{
    declare id: CreationOptional<number>;
    declare book_id: number;
    declare student_id: number;
    declare date_out: CreationOptional<Date>;
    declare entry_date: CreationOptional<Date>;
    declare id_librarian: CreationOptional<number>;
    declare getBook:BelongsToGetAssociationMixin<Book>

    public static associate(): void {
        this.belongsTo(Book, {
            foreignKey: 'book_id'
        })

        this.belongsTo(User, {
            foreignKey: 'student_id',
            as: 'student'
        })

        this.belongsTo(User, {
            foreignKey: 'id_librarian',
            as: 'librarian'
        })
    }
}

Transaction.init({
    id: {
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
    },
    book_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    student_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    date_out: {
        type: DataTypes.DATE,
    },
    entry_date: {
        type: DataTypes.DATE,
    },
    id_librarian: {
        type: DataTypes.INTEGER,
    },
}, {
    sequelize: DB.connection(),
    tableName: 'transactions',
    timestamps: false,
});