import { Model, DataTypes, InferAttributes, InferCreationAttributes, CreationOptional } from "sequelize";
import DB from "../cors/DB";
import Transaction from "./Transaction";

export default class Book extends Model<InferAttributes<Book>,InferCreationAttributes<Book>>{
    declare id: CreationOptional<number>;
    declare title:string;
    declare author:string;
    declare year:number;
    declare genre:string;
    declare stock: number;

    public static associate(): void {
       this.hasMany(Transaction,{
        foreignKey:'book_id'
       })
    }
}

Book.init({
    id:{
        type: DataTypes.INTEGER.UNSIGNED,
        autoIncrement:true,
        primaryKey:true
    },
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    year:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    genre:{
        type:DataTypes.STRING,
        allowNull:false
    },
    stock:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
},{
    sequelize: DB.connection(),
    tableName: 'books',
    timestamps:false,
});